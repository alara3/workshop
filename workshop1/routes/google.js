const express = require('express');
const router = express.Router();
const auth = require('../Codes/GoogleAuth2')
const googleApi = require('../Codes/GoogleAuth')

router.get('/google', async (req, res) => {
    console.log('google');
    let code = req.query.code;
    
    const data = await auth.getToken(code);
    const tokens = data.tokens;
  
    // add the tokens to the google api so we have access to the account
    const auth = createConnection();
    auth.setCredentials(tokens);
  
    // connect to google plus - need this to get the user's email
    const plus = getGooglePlusApi(auth);
    const me = await plus.people.get({ userId: 'me' });
  
    // get the google id and email
    const userGoogleId = me.data.id;
    const userGoogleEmail = me.data.emails && me.data.emails.length && me.data.emails[0].value;

    // return so we can login or sign up the user
    console.log({
        id: userGoogleId,
        email: userGoogleEmail,
        tokens: tokens, // you can save these to the user if you ever want to get their details without making them log in again
    });
    res.send(200).send({
        query: req.query,
    });
});

router.get('/requestGmailAuth', (req, res, next) => {
    let url = googleApi.generateUrl(['email', 'profile'])
    res.redirect(url);
});

router.get('/test', (req, res) => {
    let url = googleApi.generateUrl(['email', 'profile'])
    res.redirect(url);
    // auth();
});
    
router.get('/test2', (req, res, next)=>{
    let code = req.query.code;
    console.log(code);
    if (!code) {
        next(new Error('No code provided'))
    }
    googleApi.getUserInfo(code)
        .then( (response)=> {
            console.log(response);
            res.send(response.data)
        }).catch( (e) =>{
            next(new Error(e.message))
        })
});

module.exports = router;