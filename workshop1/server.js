const express = require('express');
const app = express();
const klap = require('./Codes/Klap.js');
const gAuth = require('./Codes/GoogleAuth.js');
const PORT = 80;
const google = require('./routes/google');
const bodyParser = require("body-parser");

app.use(bodyParser.json({limit:'100mb'}))
app.use('/google', google);

app.listen(PORT, async () => {
    console.log('Aplication running on port: ' + PORT);
    klap();
    // gAuth;   
});