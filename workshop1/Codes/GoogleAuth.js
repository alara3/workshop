  const { google } = require('googleapis')
  
  class googleApi {
		constructor(){
			this.oAuth2Client = new google.auth.OAuth2(
			'482246491233-tmotq724k6l4581pu077iuvmg7u1qj2r.apps.googleusercontent.com',
			'As3nvCO6pz7W6EMDf3bqS2m1',
			'http://localhost:5000/google/googleResponse',          
			);
		};
	
		generateUrl(scopes){
			const url = this.oAuth2Client.generateAuthUrl({
				access_type: 'offline',
				scope: scopes.join(' ')
			})
			return url;
		};
	
		async getUserInfo(code) {
			console.log(code);        
			const credentials = await this.oAuth2Client.getToken(code);
			this.oAuth2Client.setCredentials(credentials.tokens);
			const plus = google.plus({
				version: 'v1',
				auth: this.oAuth2Client,
			});
			const data = await plus.people.get({userId: 'me'});
			return data;
		};
	};
	
	module.exports = new googleApi();