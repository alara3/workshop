const fetch = require('node-fetch');
const lista = require('../Listas/EliminarKlap');

const runTest = () => {
    // console.log(lista);    
    let emails = [
        // 'lolague@gmail.com',
        // 'ligymn@gmail.com',
        'corderoeca@yahoo.com'
    ];
    emails.forEach((email,i) => {
        // customerExist('email');
        // deleteCustomer('email');
        customerExist(email);
        // deleteCustomer(email,i);
    });  
}

const deleteCustomer = async (email,i) => {
    let miInit = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'x-api-key': 'C4679EBC4A86F66E552C6D3233B36'
        }
    };

    let res = await fetch('https://payapi-prod.azurewebsites.net/api/v1/Customer', miInit)
        .then(response => {
            return response.json();
        }).catch(err => console.log(err));
    // console.log(res);
    let data = await res.Data;
    if (data) {
        let customerD = {};
        data.forEach(customer => {
            if (customer.Email === email) {
                customerD = customer;
            }
        });
        // console.log('customerD');
        // console.log(customerD);
        if (customerD.CustomerID) {
            miInit.method = 'DELETE';
            res = await fetch('https://payapi-prod.azurewebsites.net/api/v1/Customer/' + customerD.CustomerID, miInit)
                .then(response => {
                    return response.json();
                }).catch(err => console.log(err));
            // console.log('delete customer');
            // console.log(res);
            if (res.Data === 'Success') {
                console.log('delete '+email);                
            }
            if (res.Error && res.Error.HasError && res.Error.Message.includes('the customer has pending charges')) {
                miInit.method = 'GET';
                res = await fetch('https://payapi-prod.azurewebsites.net/api/v1/Customer/' + customerD.CustomerID + '/Charges', miInit)
                    .then(response => {
                        return response.json();
                    }).catch(err => console.log(err));
                // console.log('get customer charges');
                // console.log(res);
                let charges = [];
                res.Data.forEach(charge => {
                    let applied = false;
                    charge.ChargeLogs.forEach(element => {
                        if (element.IsApplied) {
                            applied = true;
                        }
                    });
                    if (!applied) {
                        charges.push(charge.ChargeID);
                    }
                });
                charges.forEach(async (charge, i) => {
                    miInit.method = 'DELETE';
                    res = await fetch('https://payapi-prod.azurewebsites.net/api/v1/Charge/' + charge, miInit)
                        .then(response => {
                            return response.json();
                        }).catch(err => console.log(err));
                    // console.log('delete charge');
                    // console.log(res);
                    if (i === charges.length - 1) {
                        // console.log('retry delete customer start');
                        miInit.method = 'DELETE';
                        res = await fetch('https://payapi-prod.azurewebsites.net/api/v1/Customer/' + customerD.CustomerID, miInit)
                            .then(response => {
                                return response.json();
                            }).catch(err => console.log(err));
                        // console.log('retry delete customer end');
                        // console.log(res);
                        console.log('delete '+email);     
                        //enviar mensaje a peter Success
                    }
                });
            }
        }
        else {
            //enviar mensaje a pedro de falso
            console.log('customer doesn\'t exist '+email);
        }
    }
};

const customerExist = async (email) => {
    let miInit = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'x-api-key': 'C4679EBC4A86F66E552C6D3233B36'
        }
    };

    let res = await fetch('https://payapi-prod.azurewebsites.net/api/v1/Customer', miInit)
        .then(response => {
            return response.json();
        }).catch(err => console.log('error customer exist obtener data',err));
    let data = res.Data;
    let exist = false;
    if (data) {
        data.forEach(element => {
            if (email === element.Email) {
                exist = true;
            }
        });
    }   
    console.log(exist);
    return exist
};

module.exports = runTest;